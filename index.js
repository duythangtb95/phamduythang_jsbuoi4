/* 1. Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần */

// input: nhập 3 số nguyên
// output: xuất 3 số theo thứ tự tăng dần

document.getElementById('Tinh1').onclick = function () {
    var a = Number((document.getElementById('so1')).value);
    var b = Number((document.getElementById('so2')).value);
    var c = Number((document.getElementById('so3')).value);

    if (a >= b && b >= c) {
        document.getElementById('ketQua1').innerHTML = a + '; ' + b + '; ' + c;
    } else if (a >= c && c >= b) {
        document.getElementById('ketQua1').innerHTML = a + '; ' + c + '; ' + b;
    } else if (b >= a && a >= c) {
        document.getElementById('ketQua1').innerHTML = b + '; ' + a + '; ' + c;
    } else if (b >= c && c >= a) {
        document.getElementById('ketQua1').innerHTML = b + '; ' + c + '; ' + a;
    } else if (c >= a && a >= b) {
        document.getElementById('ketQua1').innerHTML = c + '; ' + a + '; ' + b;
    } else if (c >= b && b >= a) {
        document.getElementById('ketQua1').innerHTML = c + '; ' + b + '; ' + a;
    }

}

// step 1: viết chương trình nhập 3 số
// step 2: viết hàm trả kết quả
// step 3: trong hàm nêu lần lượt các kết quả in ra theo từng trường hợp: 
// th1: a >= b && b >= c
// th2: a >= c && c >= b
// th3: b >= a && a >= c
// th4: b >= c && c >= a
// th5: c >= a && a >= b
// th6: c >= b && b >= a

// end bài 1

/* 2. Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ
    hỏi ai sử dụng máy. Sau đó dựa vào câu trả lời và đưa ra lời chào phù hợp. Giả sử trong gia
    đình có 4 thành viên: Bố (B), Mẹ (M), anh Trai (A) và Em gái (E)*/

// input: nhập người dùng
// output: đưa ra lời chào với dùng

document.getElementById('Tinh2').onclick = function () {

    var nguoiDung = document.getElementById('nguoiDung').value;
    console.log(nguoiDung)
    if (nguoiDung == 'B') {
        document.getElementById('ketQua2').innerHTML = 'Bố';
    } else if (nguoiDung == 'M') {
        document.getElementById('ketQua2').innerHTML = 'Mẹ';
    } else if (nguoiDung == 'A') {
        document.getElementById('ketQua2').innerHTML = 'Anh';
    } else if (nguoiDung == 'E') {
        document.getElementById('ketQua2').innerHTML = 'Em';
    }

    // switch (nguoiDung) {
    //     case 'B': {
    //         document.getElementById('ketQua2').innerHTML = 'Bố';
    //     }; break;
    //     case 'M': {
    //         document.getElementById('ketQua2').innerHTML = 'Mẹ';
    //     }; break;
    //     case 'A': {
    //         document.getElementById('ketQua2').innerHTML = 'Anh';
    //     }; break;
    //     default: {
    //         document.getElementById('ketQua2').innerHTML = 'Em';
    //     }
    // }
}
// step1: viết chương trình nhập kết quả ngưỜi dùng theo các trường hợp;
// step2: Viết hàm trả kết quả
// step3: trong hàm nếu in kết quả với các trường hợp người dùng, dùng hàm if, else if 
// end bài 2




/* 3. Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn. */

// input: nhập 3 số nguyên
// output: tính xem có bao nhiêu số chẵn bao nhiêu số lẻ


document.getElementById('Tinh3').onclick = function () {

    var soChan = 0;
    var soLe = 0;

    var somot = Number((document.getElementById('somot')).value);
    var sohai = Number((document.getElementById('sohai')).value);
    var soba = Number((document.getElementById('soba')).value);

    // somot % 2 == 0 ? soChan += 1 : soLe += 1;
    // sohai % 2 == 0 ? soChan += 1 : soLe += 1;
    // soba % 2 == 0 ? soChan += 1 : soLe += 1;

    if (somot % 2 == 0) {
        soChan += 1;
    } else {
        soLe += 1;
    }

    if (sohai % 2 == 0) {
        soChan += 1;
    } else {
        soLe += 1;
    }

    if (soba % 2 == 0) {
        soChan += 1;
    } else {
        soLe += 1;
    }

    document.getElementById('soChan').innerHTML = soChan;
    document.getElementById('soLe').innerHTML = soLe;
}


// step1: viết chương trình nhập 3 số
// step2: viết hàm trả ra kết quả
// step3: trong hàm ta gọi biến chẵn và biến lẻ. Dùng biểu thức 3 ngôi, nếu 1 số là chẵn thì biến chẵn cộng 1, là số lẻ biến lẻ cộng 1 rồi in ra kết quả

// end bài 3

/* 4. Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì?
• Ví dụ: a=2, b=2, c=1 => Tam giác cân
• a = 3, b=3 c=3 => Tam giác đều
• a = 3, b = 4, c=5 => Tam giác vuông (đinh lý Pytago)*/

// input: nhập 3 cạnh của 1 tam giác
// output: trả kết quả của tam giác


document.getElementById('Tinh4').onclick = function () {

    var x = Number((document.getElementById('canhmot')).value);
    var y = Number((document.getElementById('canhhai')).value);
    var z = Number((document.getElementById('canhba')).value);


    if (((x + y) <= z) || ((x + z) <= y) || ((z + y) <= x)) {
        document.getElementById('Ketqua4').innerHTML = '3 cạnh trên không phải của một tam giác';
    } else if ((x == y) && (y == z)) {
        document.getElementById('Ketqua4').innerHTML = 'Tam giác đều';
    } else if ((((x ** 2 == (y ** 2 + z ** 2)) && y == z) || ((y ** 2 == (x ** 2 + z ** 2)) && x == z) || (z ** 2 == (y ** 2 + x ** 2)) && (x == y))) {
        document.getElementById('Ketqua4').innerHTML = 'Tam giác vuông cân';
    } else if ((x == y) || (y == z) || (z == x)) {
        document.getElementById('Ketqua4').innerHTML = 'Tam giác cân';
    } else if ((x ** 2 == (y ** 2 + z ** 2)) || (y ** 2 == (x ** 2 + z ** 2)) || (z ** 2 == (y ** 2 + x ** 2))) {
        document.getElementById('Ketqua4').innerHTML = 'Tam giác vuông';
    } else {
        document.getElementById('Ketqua4').innerHTML = 'Tam giác thường';
    }
}

// step1: viết chương trình nhập 3 cạnh
// step2: viết hàm xét các trường hợp để trả giá trị
// step3: trong hàm xét các trường hợp tam giác theo điều kiện của từng loại. đầu tiên xét về điều kiện tam giác sau đó xét tam giác đều, tam giác vuông cân, tam giác cân, tam giác vuông, nếu không thảo mãn thì sẽ xét là tam giác thường. Lưu ý xét trường hợp vuông cân trước 2 trường hợp cân và vuông vì nó có điều kiện thảo mãn hai điều kiện củ trường hợp sau.
// step4: in kết quả ra màn hình

// end bài 4